//
//  InterfaceController.swift
//  SushiWatch Extension
//
//  Created by Akhil S Raj on 2019-11-01.
//  Copyright © 2019 Parrot. All rights reserved.
//

import WatchKit
import Foundation
import WatchConnectivity

class InterfaceController: WKInterfaceController, WCSessionDelegate{
    
    @IBOutlet weak var startButton: WKInterfaceButton!
    @IBOutlet weak var leftButton: WKInterfaceButton!
    @IBOutlet weak var rightButton: WKInterfaceButton!
    @IBOutlet weak var powerUpButton: WKInterfaceButton!
    
    var timer = Timer()
    var time = 0
    var isGameOver = false
    var username = ""
    
    override func awake(withContext context: Any?) {
        super.awake(withContext: context)
        if WCSession.isSupported() {
            WCSession.default.delegate = self
            WCSession.default.activate()
            print("Watch connection activated")
        }
    }
    
    @IBAction func startButtonPressed() {
        print("Start button pressed message sent to phone")
        if (WCSession.default.isReachable == true) {
            let message = ["gamestatus":"start"] as [String : Any]
            WCSession.default.sendMessage(message, replyHandler:nil)
        }
        if self.isGameOver {
            self.leftButton.setHidden(false)
            self.rightButton.setHidden(false)
            self.isGameOver = false
        }
    }
    
    @IBAction func leftButtonPressed() {
        print("Left button pressed message sent to phone")
        if (WCSession.default.isReachable == true) {
            let message = ["direction":"left"] as [String : Any]
            WCSession.default.sendMessage(message, replyHandler:nil)
        }
    }
    
    @IBAction func rightButtonPressed() {
        print("Right button pressed message sent to phone")
        if (WCSession.default.isReachable == true) {
            let message = ["direction":"right"] as [String : Any]
            WCSession.default.sendMessage(message, replyHandler:nil)
        }
    }
    
    @IBAction func addPowerUpPressed() {
        print("Right button pressed message sent to phone")
        if (WCSession.default.isReachable == true) {
            let message = ["powerup":"powerup"] as [String : Any]
            WCSession.default.sendMessage(message, replyHandler:nil)
        }
    }
    
    func sendUserDetails() {
        print("User details send to phone\(self.username)")
        if (WCSession.default.isReachable == true) {
            let message = ["username":self.username] as [String : Any]
            WCSession.default.sendMessage(message, replyHandler:nil)
        }
    }
    
    override func willActivate() {
        super.willActivate()
        self.powerUpButton.setHidden(true)
    }
    
    override func didDeactivate() {
        super.didDeactivate()
    }
    
    func session(_ session: WCSession, didReceiveMessage message: [String : Any]) {
        print("Received a message from the phone: \(message)")
        DispatchQueue.main.async {
            print("\nMessage Received: \(message)")
            let h0 = { print("ok")}
            let action = WKAlertAction(title: "Ok", style: .default, handler:h0)
            let warningTimer = message["warning"] as? String
            let powerUp = message["powerup"] as? String
            if warningTimer == "timerwarning" {
                self.presentAlert(withTitle: "Warning", message: "Game ends soon ", preferredStyle: .alert, actions: [action])
            }else if warningTimer == "gameover"{
                self.presentAlert(withTitle: "Warning", message: "Game Over", preferredStyle: .alert, actions: [WKAlertAction(title: "OK", style: .default){
                    let suggestedResponses = ["Michael", "John", "Cristiano", "Leo"]
                    self.presentTextInputController(withSuggestions: suggestedResponses, allowedInputMode: .plain) { (results) in
                        
                        if (results != nil && results!.count > 0) {
                            self.username = (results?.first as? String)!
                            self.sendUserDetails()
                            self.leftButton.setHidden(true)
                            self.rightButton.setHidden(true)
                            self.isGameOver = true

                        }
                    }
                    
                    }])
                
            }
            if powerUp == "powerup" {
                self.powerUpButton.setHidden(false);
                self.runTimer()
            }
        }
    }
    
    func runTimer() {
        print("Inside run timer")
        self.timer = Timer.scheduledTimer(timeInterval: 1, target: self,   selector: (#selector(InterfaceController.updateTimer)), userInfo: nil, repeats: true)
    }
    
    @objc func updateTimer(){
        self.time = time + 1
        if self.time == 2 {
            self.powerUpButton.setHidden(true)
            self.timer.invalidate()
            self.time = 0
        }
    }
    
    func session(_ session: WCSession, activationDidCompleteWith activationState: WCSessionActivationState, error: Error?) {
    }
}
