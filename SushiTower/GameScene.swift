//
//  GameScene.swift
//  SushiTower
//
//  Created by Parrot on 2019-02-14.
//  Copyright © 2019 Parrot. All rights reserved.
//

import SpriteKit
import GameplayKit
import WatchKit
import WatchConnectivity
import Firebase
import FirebaseStorage


class GameScene: SKScene, WCSessionDelegate {
    
    let cat = SKSpriteNode(imageNamed: "character1")
    let sushiBase = SKSpriteNode(imageNamed:"roll")
    
    // Make a tower
    var sushiTower:[SushiPiece] = []
    let SUSHI_PIECE_GAP:CGFloat = 80
    var catPosition = "left"
    
    // Show life and score labels
    let lifeLabel = SKLabelNode(text:"Lives: ")
    let scoreLabel = SKLabelNode(text:"Score: ")
    
    let leaderBoard = SKSpriteNode(imageNamed: "button")

    var lives = 5
    var score = 0
    
    var catDirection = ""
    
    let timerImage = SKSpriteNode(imageNamed: "life")
    let timer_bg = SKSpriteNode(imageNamed: "life_bg")
    
    var timer = Timer()
    
    var timerStarted = false
    
    var seconds = 25
    
    var firstPowerUpTime = 0
    var secondPowerUpTIme = 0
    var powerUpCount = 0
    var username = ""
    
    var database:Firestore!
    
    override func sceneDidLoad() {
        database = Firestore.firestore()
        //WatchKIt Integration part
        if WCSession.isSupported() {
            print("\nPhone supports WCSession")
            WCSession.default.delegate = self
            WCSession.default.activate()
            print("\nSession activated")
        }
        else {
            print("Phone does not support WCSession")
            print("\nPhone does not support WCSession")
        }
    }
    // -----------------------
    // MARK: PART 1: ADD SUSHI TO GAME
    // -----------------------
    func spawnSushi() {
        // 1. Make a sushi
        let sushi = SushiPiece(imageNamed:"roll")
        // 2. Position sushi 10px above the previous one
        if (self.sushiTower.count == 0) {
            // Sushi tower is empty, so position the piece above the base piece
            sushi.position.y = sushiBase.position.y
                + SUSHI_PIECE_GAP
            sushi.position.x = self.size.width*0.5
        }
        else {
            // OPTION 1 syntax: let previousSushi = sushiTower.last
            // OPTION 2 syntax:
            let previousSushi = sushiTower[self.sushiTower.count - 1]
            sushi.position.y = previousSushi.position.y + SUSHI_PIECE_GAP
            sushi.position.x = self.size.width*0.5
        }
        // 3. Add sushi to screen
        addChild(sushi)
        // 4. Add sushi to array
        self.sushiTower.append(sushi)
    }
    
    override func didMove(to view: SKView) {
        
        //self.runTimer()
        // add background
        let background = SKSpriteNode(imageNamed: "background")
        background.size = self.size
        background.position = CGPoint(x: self.size.width / 2, y: self.size.height / 2)
        background.zPosition = -1
        addChild(background)
        
        // add cat
        cat.position = CGPoint(x:self.size.width*0.25, y:100)
        addChild(cat)
        
        // add base sushi pieces
        sushiBase.position = CGPoint(x:self.size.width*0.5, y: 100)
        addChild(sushiBase)
        
        // build the tower
        self.buildTower()
        
        // Game labels
        self.scoreLabel.position.x = 75
        self.scoreLabel.position.y = size.height - 50
        self.scoreLabel.fontName = "Avenir"
        self.scoreLabel.fontSize = 20
        self.scoreLabel.fontColor = UIColor.red
        
        addChild(scoreLabel)
        
        // Life label
        self.lifeLabel.position.x = 75
        self.lifeLabel.position.y = size.height - 75
        self.lifeLabel.fontName = "Avenir"
        self.lifeLabel.fontSize = 20
        self.lifeLabel.fontColor = UIColor.red
        addChild(lifeLabel)
        
        self.timerImage.position.x = size.width - 150
        self.timerImage.position.y = size.height - 75
        self.timerImage.zPosition = 3
        self.timerImage.anchorPoint = CGPoint(x: 0,y: 0)
        addChild(timerImage)
        
        self.timer_bg.position.x = size.width - 160
        self.timer_bg.position.y = size.height - 85
        self.timer_bg.zPosition = 1
        self.timer_bg.anchorPoint = CGPoint(x: 0,y: 0)
        addChild(timer_bg)
        
        self.leaderBoard.position.x = 200
        self.leaderBoard.position.y = size.height - 50
        addChild(leaderBoard)
    }
    
    func buildTower() {
        for _ in 0...10 {
            self.spawnSushi()
        }
    }
    
    func session(_ session: WCSession, didReceiveMessage message: [String : Any]) {
        DispatchQueue.main.async {
            print("\nMessage Received: \(message)")
            let gameStatus = message["gamestatus"] as? String
            let powerUp = message["powerup"] as? String
            let username = message["username"] as? String
            let catMove = message["direction"] as? String
            if gameStatus == "start" {
                if !self.timerStarted{
                    self.runTimer()
                    self.timerStarted = true
                }
            }else if powerUp == "powerup" {
                self.addPowerUp()
            }
            else if catMove != "" && catMove != nil {
                self.catDirection = message["direction"] as! String
                self.moveCat(direction: self.catDirection)
            }
            
            if username != "" && username != nil {
                
                self.saveLeaderBoard(username: username!)
            }
            
        }
    }
    
    func moveCat(direction: String) {
        if (self.catDirection == "left") {
            print("TAP LEFT")
            // 2. person clicked left, so move cat left
            cat.position = CGPoint(x:self.size.width*0.25, y:100)
            // change the cat's direction
            let facingRight = SKAction.scaleX(to: 1, duration: 0)
            self.cat.run(facingRight)
            // save cat's position
            self.catPosition = "left"
        }
        else if self.catDirection == "right"{
            print("TAP RIGHT")
            // 2. person clicked right, so move cat right
            cat.position = CGPoint(x:self.size.width*0.85, y:100)
            // change the cat's direction
            let facingLeft = SKAction.scaleX(to: -1, duration: 0)
            self.cat.run(facingLeft)
            // save cat's position
            self.catPosition = "right"
        }
        
        let image1 = SKTexture(imageNamed: "character1")
        let image2 = SKTexture(imageNamed: "character2")
        let image3 = SKTexture(imageNamed: "character3")
        
        let punchTextures = [image1, image2, image3, image1]
        
        let punchAnimation = SKAction.animate(
            with: punchTextures,
            timePerFrame: 0.1)
        
        self.cat.run(punchAnimation)
        
        // ------------------------------------
        // MARK: UPDATE THE SUSHI TOWER GRAPHICS
        //  When person taps mouse,
        //  remove a piece from the tower & redraw the tower
        // -------------------------------------
        let pieceToRemove = self.sushiTower.first
        if (pieceToRemove != nil) {
            // SUSHI: hide it from the screen & remove from game logic
            pieceToRemove!.removeFromParent()
            self.sushiTower.remove(at: 0)
            
            // SUSHI: loop through the remaining pieces and redraw the Tower
            for piece in sushiTower {
                piece.position.y = piece.position.y - SUSHI_PIECE_GAP
            }
            
            // To make the tower inifnite, then ADD a new piece
            self.spawnSushi()
        }
        
        if (self.sushiTower.count > 0) {
            // 1. if CAT and STICK are on same side - OKAY, keep going
            // 2. if CAT and STICK are on opposite sides -- YOU LOSE
            let firstSushi:SushiPiece = self.sushiTower[0]
            let chopstickPosition = firstSushi.stickPosition
            
            if (catPosition == chopstickPosition) {
                // cat = left && chopstick == left
                // cat == right && chopstick == right
                print("Cat Position = \(catPosition)")
                print("Stick Position = \(chopstickPosition)")
                print("Conclusion = LOSE")
                print("------")
                
                self.lives = self.lives - 1
                self.lifeLabel.text = "Lives: \(self.lives)"
            }
            else if (catPosition != chopstickPosition) {
                // cat == left && chopstick = right
                // cat == right && chopstick = left
                print("Cat Position = \(catPosition)")
                print("Stick Position = \(chopstickPosition)")
                print("Conclusion = WIN")
                print("------")
                
                self.score = self.score + 10
                self.scoreLabel.text = "Score: \(self.score)"
            }
        }
        else {
            print("Sushi tower is empty!")
        }
    }
    
    func runTimer() {
        print("Inside run timer")
        self.firstPowerUpTime = generateRandomPowerUpTIme()
        self.secondPowerUpTIme = generateRandomPowerUpTIme()
        self.timer = Timer.scheduledTimer(timeInterval: 1, target: self,   selector: (#selector(GameScene.updateTimer)), userInfo: nil, repeats: true)
    }
    
    @objc func updateTimer() {
       // print("Inside update timer")
        self.seconds = self.seconds - 1
        
        let reduceTimerWidth = self.timerImage.size.width/20
        //self.timerImage.size.width =  self.timerImage.size.width -  self.timerImage.size.width * 0.04
        self.timerImage.size =  CGSize(width:  self.timerImage.size.width - reduceTimerWidth, height: self.timerImage.size.height)
        if seconds == 15 || seconds == 10 || seconds == 5 {
            sendNotificationToWatch()
        }
        if seconds == 0 {
            sendGameOverNotification()
            timer.invalidate()
            seconds = 25
            
        }
        
        if self.seconds == firstPowerUpTime || self.seconds == secondPowerUpTIme {
            if self.powerUpCount <= 2 {
                generatePowerUps()
            }
        }
    }
    
    func sendNotificationToWatch(){
        if (WCSession.default.isReachable) {
            let message = ["warning": "timerwarning"]
            WCSession.default.sendMessage(message, replyHandler: nil)
            // output a debug message to the UI
            print("\nMessage sent to watch on time \(self.seconds)")
            
        } else {
            print("PHONE: Cannot reach watch")
        }
    }
    
    
    func sendGameOverNotification(){
        if (WCSession.default.isReachable) {
            let message = ["warning": "gameover"]
            WCSession.default.sendMessage(message, replyHandler: nil)
            // output a debug message to the UI
            print("\nMessage sent to watch")
            
        } else {
            print("PHONE: Cannot reach watch")
        }
    }
    
    func generatePowerUps(){
        print("Generate powerups invoked")
        if self.powerUpCount <= 2 {
            if (WCSession.default.isReachable) {
                let message = ["powerup": "powerup"]
                WCSession.default.sendMessage(message, replyHandler: nil)
                // output a debug message to the UI
                print("\nMessage sent to watch")
                
            } else {
                print("PHONE: Cannot reach watch")
            }
        }
        
        
    }
    
    func addPowerUp(){
        print("Add powerup invoked")
        
        self.powerUpCount = self.powerUpCount + 1
        self.seconds = self.seconds + 10
        self.timerImage.size =  CGSize(width:  self.timerImage.size.width + 10 * self.timerImage.size.width/25, height: self.timerImage.size.height)
        
    }
    
    func generateRandomPowerUpTIme() -> Int {
        return Int.random(in: 0...20)
    }
    
    func saveLeaderBoard(username: String){
        print("Save leaderboard invoked")
        var ref: DocumentReference? = nil
        ref = database.collection("leaderboard").addDocument(data: [
            username: self.score
        ]) { err in
            if let err = err {
                print("Error adding document: \(err)")
            } else {
                print("Document added with ID: \(ref!.documentID)")
            }
        }
    }
    
    func retrieveLeaderBoard(){
        database.collection("leaderboard").getDocuments() { (querySnapshot, err) in
            if let err = err {
                print("Error getting documents: \(err)")
            } else {
                for document in querySnapshot!.documents {
                    print("\(document.documentID) => \(document.data())")
                }
            }
        }
    }
    
    override func update(_ currentTime: TimeInterval) {}
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        let touch = touches.first
        if (touch == nil) {
            return
        }
        // print("Inside touchesbegan")
        let touchLocation = touch!.location(in: self)
        
        if self.leaderBoard.contains(touchLocation) {
           self.retrieveLeaderBoard()
        }
        
        
    }
    
    func session(_ session: WCSession, activationDidCompleteWith activationState: WCSessionActivationState, error: Error?) {}
    
    func sessionDidBecomeInactive(_ session: WCSession) {}
    
    func sessionDidDeactivate(_ session: WCSession) {}
    
    
}
